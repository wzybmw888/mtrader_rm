# mtrader_rm

#### 介绍
期货多账户风控管理系统，用于仓位亏损控制，统一后台风控，避免极端行情出现导致的意外亏损。
![img.png](static/img.png)


#### 安装教程

1.  pip install -r requriements.txt


#### 使用说明

1. 启动程序：python main.py
2. 打包项目： pyinstaller main.spec
3. 导出requirements.txt：pip list --format=freeze > requirements.txt

#### 常见错误
1.  ERROR: Failed building wheel for ta-lib
可以通过wheel安装talib,可查看链接教程：https://zhuanlan.zhihu.com/p/345127194


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

