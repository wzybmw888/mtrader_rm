import qdarkstyle
import ctypes

from qdarkstyle.light.palette import LightPalette
from vnpy.trader.setting import SETTINGS
from PySide6.QtWidgets import QApplication
from mquant.trader.config import ICON_PATH
from mquant.ui.mainwindow import MainWindow
from PySide6 import QtGui

if __name__ == "__main__":
    app = QApplication([])
    window = MainWindow()

    # or in new API
    # app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside6"))
    app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside6", palette=LightPalette()))
    # Set up font
    font: QtGui.QFont = QtGui.QFont(SETTINGS["font.family"], SETTINGS["font.size"])
    app.setFont(font)

    # Set up icon
    icon: QtGui.QIcon = QtGui.QIcon(ICON_PATH)
    app.setWindowIcon(icon)
    # 设置进程ID（否则任务栏图标无法显示）
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID("mtrader")
    window.showMaximized()
    app.exec()
