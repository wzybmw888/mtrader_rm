from typing import List

from mquant.trader.object import AccountModelData, RiskManagerModelData
from mquant_sqlite.sqlite_database import SqliteDatabase


def test_account_data():
    db = SqliteDatabase()
    accounts = db.load_account_data()
    print(accounts)
    assert isinstance(accounts, List)


def test_riskManager_data():
    db = SqliteDatabase()
    riskManagers = db.load_riskManager_data()
    print(riskManagers)
    assert isinstance(riskManagers, List)


def test_riskManager_data_by_account():
    db = SqliteDatabase()
    riskManagers = db.load_riskManager_data_by_account("218042")
    print(riskManagers)


def test_inser_account():
    account1 = AccountModelData(
        id=1,
        username="wzy",
        account="218042",
        password="wangqiang@123",
        broker=9999,
        trade_server="180.168.146.187:10202",
        quotation_server="180.168.146.187:10212",
        product_name="simnow_client_test",
        authorization_code="0000000000000000",
        init_balance=500000
    )
    account2 = AccountModelData(
        id=2,
        username="lyh",
        account="218097",
        password="wangqiang@123",
        broker=9999,
        trade_server="180.168.146.187:10202",
        quotation_server="180.168.146.187:10212",
        product_name="simnow_client_test",
        authorization_code="0000000000000000",
        init_balance=500000
    )
    db = SqliteDatabase()
    res = db.save_account_data([account1, account2])
    assert res == True


def test_inser_risk():
    rm1 = RiskManagerModelData(
        id=1,
        username="wzy",
        account="218042",
        max_loss=0.1,
        sleep=0,
    )
    rm2 = RiskManagerModelData(
        id=2,
        username="lyh",
        account="218097",
        max_loss=0.2,
        sleep=0,
    )
    db = SqliteDatabase()
    res = db.save_riskManager_data([rm1, rm2])
    assert res == True
