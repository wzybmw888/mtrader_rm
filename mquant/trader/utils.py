import json
from datetime import datetime


def remove_keys_from_dict(dictionary: dict, keys: list) -> dict:
    """
    从字典中删除一系列键，返回删除后的字典
    dictionary:字典
    keys:删除的键
    """
    return {k: v for k, v in dictionary.items() if k not in keys}



