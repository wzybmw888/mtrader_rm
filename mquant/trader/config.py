import os


def get_project_root():
    current_file = os.path.abspath(__file__)  # 获取当前脚本文件的绝对路径
    project_root = os.path.dirname(os.path.dirname(os.path.dirname(current_file)))
    return project_root


ICON_PATH = os.path.join(os.path.join(get_project_root(), "static"), "project.ico")

DB_PATH = os.path.join(get_project_root(), "database.db")
